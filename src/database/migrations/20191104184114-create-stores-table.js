"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("stores", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      license_number: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      operation_type: {
        type: Sequelize.STRING,
        allowNull: false
      },
      establishment_type: {
        type: Sequelize.STRING,
        allowNull: false
      },
      entity_name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      dba_name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("stores");
  }
};
