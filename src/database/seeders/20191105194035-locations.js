"use strict";
const csv = require("csvtojson");
const path = require("path");
const csvFilePath = path.resolve(
  __dirname,
  "..",
  "..",
  "config",
  "dataset.csv"
);
const headers = [
  "country",
  "license_number",
  "operation_type",
  "establishment_type",
  "entity_name",
  "dba_name",
  "street_number",
  "street_name",
  "address1",
  "address2",
  "city",
  "state",
  "zip_code",
  "footage",
  "location"
];
const params = {
  trim: true,
  noheader: true,
  headers
};
const items = [];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await csv(params)
      .fromFile(csvFilePath)
      .then(jsonObj => {
        jsonObj.forEach((item, index) => {
          if (Number(item.license_number)) {
            let locationString;

            if (item.location) {
              locationString = JSON.parse(
                item.location
                  .replace(/'/g, '"')
                  .replace("False", "false")
                  .replace(/, "human_address":.*}"/g, "")
              );
            }

            const payload = {
              store_id: index,
              country: item.country,
              street_number: item.street_number,
              street_name: item.street_name,
              city: item.city,
              state: item.state,
              zip_code: Number(item.zip_code),
              footage: Number(item.footage),
              latitude:
                locationString && locationString.latitude
                  ? Number(locationString.latitude)
                  : 0,
              longitude:
                locationString && locationString.longitude
                  ? Number(locationString.longitude)
                  : 0,
              createdAt: new Date(),
              updatedAt: new Date()
            };
            items.push(payload);
          }
        });
      });
    return queryInterface.bulkInsert("locations", items, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("locations", null, {});
  }
};
