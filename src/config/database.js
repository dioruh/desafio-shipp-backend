module.exports = {
  dialect: "sqlite",
  storage: "./backend.sqlite3",
  definde: {
    timestamps: true,
    underscored: true
  }
};
