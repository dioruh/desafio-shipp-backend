const Store = require("../models/Store");

const add = async (req, res) => {
  const {
    license_number,
    operation_type,
    establishment_type,
    entity_name,
    dba_name
  } = req.body;

  const store = await Store.create({
    license_number,
    operation_type,
    establishment_type,
    entity_name,
    dba_name
  });

  return res.json(store);
};

const index = async (req, res) => {
  const stores = await Store.findAll();
  return res.json(stores);
};

module.exports = {
  add,
  index
};
