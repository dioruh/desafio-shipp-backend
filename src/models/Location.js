const { Model, DataTypes } = require("sequelize");

class Location extends Model {
  static init(sequelize) {
    super.init(
      {
        street_number: DataTypes.STRING,
        country: DataTypes.STRING,
        street_name: DataTypes.STRING,
        city: DataTypes.STRING,
        state: DataTypes.STRING,
        zip_code: DataTypes.INTEGER,
        footage: DataTypes.INTEGER,
        latitude: DataTypes.DOUBLE,
        longitude: DataTypes.DOUBLE
      },
      { sequelize }
    );
  }

  static associate(models) {
    this.belongsTo(models.Store, { foreignKey: "store_id", as: "store" });
  }
}

module.exports = Location;
