const { Model, DataTypes } = require("sequelize");

class Log extends Model {
  static init(sequelize) {
    super.init(
      {
        latitude: DataTypes.DOUBLE,
        longitude: DataTypes.DOUBLE,
        status_code: DataTypes.INTEGER,
        returned_stores: DataTypes.INTEGER
      },
      { sequelize }
    );
  }
}

module.exports = Log;
